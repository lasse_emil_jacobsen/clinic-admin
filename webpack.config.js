var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var path = require('path');

module.exports = [{
		entry: {
			app: [path.resolve('./public/react/entry.js')]
		},
		output: {
			path: path.resolve(__dirname, '/public/build'),
			filename: 'bundle.js'
		},
		module: {
			loaders: [
				{
					test: /\.jsx?$/,
					exclude: /(node_modules|bower_components)/,
					loader: 'babel',
					query: {
						presets: ['react', 'es2015']
					}
				},
				{
					test: /\.scss$/,
					loader: 'style!css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!sass'
				},
				{
					test: /\.jpe?g$|\.gif$|\.png$/i,
					loader: "file-loader?name=/img/[name].[ext]"
				}
			]
		}
	}/*,
	{
		entry: {
			css: './public/react/imports.scss'
		},
		output: {
			path: './public/build',
			filename: 'styles.css'
		},
		module: {
			loaders: [{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!sass')
			}]

			/*loader: [{
					test: /\.scss$/,
					loader: ExtractTextPlugin.extract('style-loader', 'css!sass')
					/*loaders: [
						'style?sourceMap',
						'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
						'sass?sourceMap'
					]
					//loader: ExtractTextPlugin.extract('style-loader', 'css!sass')
					//loader: ExtractTextPlugin.extract('style!css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass')
				},
				{
					test: /\.jpe?g$|\.gif$|\.png$/i,
					loader: "url-loader"
				}
			]
		},
		plugins: [
			new ExtractTextPlugin('styles.css')
		]
	}
	*/
	/*{
		entry: {
			css: './public/css/style.scss'
		},
		output: {
			path: './public/build',
			filename: 'styles.css'
		},

		plugins: [
			new BrowserSyncPlugin({
				// browse to http://localhost:3000/ during development,
				// ./public directory is being served
				host: 'localhost',
				port: 3000,
				server: { baseDir: ['public'] }
				//proxy: 'http://localhost:3000/'
			})
		]
	}*/
];


/*
var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : null,
  entry: {
    "css": "./public/stylesheets/entry.scss",
    "react": "./public/react/main.js"
  },
  module: {
  loaders: [
    {
      test: /\.scss$/,
      loaders: ["style", "css", "sass"]
    },
    {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015']
      }
    }
  ]
},
  output: {
    path: "./public",
    filename: "core.min.js"
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
};
*/
