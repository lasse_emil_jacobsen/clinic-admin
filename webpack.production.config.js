var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');

module.exports = [{
    entry: {
      js: './public/react/entry.js'
    },
    output: {
        path: './public/build',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style-loader', '!css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!sass')
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$/i,
                loader: "file-loader?name=/img/[name].[ext]"
            }
        ]
    },
    resolve: {
          alias: {
              'react': 'react-lite',
              'react-dom': 'react-lite'
          }
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings: false
          }
       }),
       new webpack.DefinePlugin({
          "process.env": {
             NODE_ENV: JSON.stringify("production")
          }
       }),
       new ExtractTextPlugin('styles.css')
    ]
}/*,
{
  entry: {
     css: './public/css/style.scss'
  },
  output: {
    path: './public/build',
    filename: 'styles.css'
  },
  module: {
    loaders: [
    {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css!sass')
    },
    {
        test: /\.jpe?g$|\.gif$|\.png$/i,
        loader: "url-loader"
    }
  ]
  },
  plugins: [
      new ExtractTextPlugin('styles.css'),
  ]
}*/];
