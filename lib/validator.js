const validator = require('validator');

const validate = (item, options) => {
	const keys = Object.keys(options);

	const errors = [];

	keys.forEach((key) => {
		if (!item[key]) {
			errors.push(key + ' is missing');
		} else if (!options[key].validate(item[key])) {
			errors.push(options[key].error);
		}
	});

	return errors;
};

module.exports = {
	validator,
	validate
};
