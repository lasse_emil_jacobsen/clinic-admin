const bcrypt = require('bcryptjs');

const operations = {
	createSaltedHash: (password) => {
		return new Promise((resolve, reject) => {
			bcrypt.genSalt(10, (err, salt) => {
				if (err) reject(err);
				bcrypt.hash(password, salt, (err, hash) => {
					if (err) reject(err);
					resolve({hash, salt});
				});
			});
		});
	},
	createHash: (password, salt) => {
		return new Promise((resolve, reject) => {
			bcrypt.hash(password, salt, (err, hash) => {
				if (err) reject(err);
				resolve(hash);
			});
		});
	}
};

module.exports = operations;
