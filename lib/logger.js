const moment = require('moment');

const db = require('../mongo/logs.js');

// replace with redis or similar database service to persist errors/history if hosting server should reboot or something.
let historyBatch = [];
let errorBatch = [];

const operations = {
	event: (event) => {
		if(process.env.NODE_ENV === 'development') {
			console.log('event batched: ', event);
		}
		historyBatch.push({
			event,
			date: moment().format()
		});
	},
	error: (error) => {
		if(process.env.NODE_ENV === 'development') {
			console.log('error batched: ', error);
		}
		errorBatch.push({
			error,
			date: moment().format()
		});
	},
	emptyBatches: () => {

		console.log(historyBatch.length);
		console.log(errorBatch.length);

		const historyPromise = historyBatch.length > 0 ? db.saveHistory(historyBatch) : Promise.resolve('empty batch');
		const errorPromise = errorBatch.length > 0 ? db.saveErrors(errorBatch) : Promise.resolve('empty batch2');

		console.log(historyPromise, errorPromise);

		return new Promise((resolve, reject) => {
			Promise.all([historyPromise, errorPromise]).then((status1, status2) => {
				historyBatch.length = 0;
				errorBatch.length = 0;
				resolve('batch complete');
			}).catch(err =>reject(err));
		});
	}
};

module.exports = operations;
