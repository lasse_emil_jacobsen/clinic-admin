const express = require('express');
const router = express.Router();
const path = require('path');
const jwt = require('jsonwebtoken');
const jwtMiddleware = require('express-jwt');

const jwtConfig = {
	expiresIn: '10m',
	audience: 'Skullcave',
	issuer: 'Lotze A/S'
};

const jwtSecret = 'temporary';

const auth = require(global.__root + '/api/protected/authentication/authentication.js');

router.post('/login', (req, res) => {

	const { username, password } = req.body;

	auth.getUser(username, password).then(user => {
		if(user) {
			user.iat = new Date();
			const token = jwt.sign({user: user}, jwtSecret, jwtConfig);
			res.cookie('token', token, { maxAge: 90000, httpOnly: true});
			res.send({
				success: true,
				payload: user,
				endpoint: '/login'
			});
		} else {
			throw new Error("Database has no error, but user could not be found");
		}
	}).catch(err => {
		global.log.error(`/login endpoint: ${err}`);
		res.send({
			success: false,
			payload: err,
			endpoint: '/login'
		});
	});

});

router.post('/create', (req, res) => {
	const { email, username, password } = req.body;

	auth.createUser(req.body, true).then(resp => {
		resp.iat = new Date();
		const token = jwt.sign({resp}, jwtSecret, jwtConfig);
		res.cookie('token', token, { maxAge: 90000, httpOnly: true});
		res.send({
			success: true,
			payload: resp,
			endpoint: '/create'
		});
	}).catch(err => {
		global.log.error(`/create endpoint: ${err}`);
		res.send({
			success: false,
			payload: err,
			endpoint: '/create'
		});
	});
});

// Protected api:
router.post('/protected/*', jwtMiddleware({
	secret: jwtSecret,
	getToken: (req) => {
		if(req.cookies) {
			return req.cookies.token;
		} else {
			return null;
		}
	}
}));

// replace with redis
const blacklist = {};

router.post('/protected/*', (req, res, next) => {
	const { user } = req.user;

	console.log(user, 'user');
	console.log(req.cookies.token, 'cookie');
	console.log(blacklist, 'blacklist');

	if(blacklist[user.username] && blacklist[user.username] === user.iat) {
		next({name: "RevokedTokenError"});
	} else {
		next();
	}
});

router.post('/protected/secret', (req, res) => {
	res.send("Super secret path");
});

router.post('/protected/logout', (req, res) => {
	const { user } = req.user;

	blacklist[user.username] = user.iat;

	res.send("Token revoked");
});


module.exports = router;
