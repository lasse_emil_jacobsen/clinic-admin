import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import user from './containers/UserModule/reducer';

const data = (state = {
   user: 'test'
}, action) => {
  switch(action.type) {
    default:
      return state;
  }
};

const store = createStore(combineReducers({
   user,
   data
}), window.devToolsExtension && window.devToolsExtension()); //createStore(reducer, window.devToolsExtension && window.devToolsExtension());

export default store;
