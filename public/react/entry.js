import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import Match from 'react-router/Match';
import Router from 'react-router/BrowserRouter';
import CSSModules from 'react-css-modules';

import globalStyles from './css/global.scss';
import styles from './entry.scss';

import {Provider} from 'react-redux';

import store from './store.js';

import Header from './containers/header/Header.js';

import FrontPage from './pages/FrontPage/FrontPage.js';
import Training from './pages/Training/Training.js';
import Contact from './pages/Contact/Contact.js';
import Admin from './pages/Admin/Admin.js';

class Routes extends Component {
   render() {
      return (
         <Provider store={store}>
            <div>
               <Router>
                  <div styleName='content'>
                     <Header/>
                     
                     <Match exactly pattern="/" component={FrontPage} />
                     <Match pattern="/programmer" component={Training} />
                     <Match pattern="/kontakt" component={Contact} />
                     <Match pattern="/admin" component={Admin} />
                     <Match pattern="/forloeb" component={Admin} />
                  </div>
               </Router>
            </div>
         </Provider>
      );
   }
}

ReactDOM.render(React.createElement(CSSModules(Routes, styles)), document.getElementById('react-hook'));
