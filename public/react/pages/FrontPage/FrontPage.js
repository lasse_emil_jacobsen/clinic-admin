import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './frontpage.scss';

import NavBar from '../../containers/navbar/NavBar';
import UserModule from '../../containers/usermodule/UserModule';
import Cell from '../../containers/cell/Cell';
import CellContent from '../../containers/cell/CellContent';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class FrontPage extends Component {
   render() {
      return (
         <div>
            <div className="grid">
               <Cell cols="4">
                  <CellContent> Velkommen til Søernes Fysioterapi </CellContent>
               </Cell>
               <Cell cols="4">
                  <CellContent> Om os </CellContent>
               </Cell>
               <Cell cols="4 right">
                  <CellContent>
                     <h2> Start dit forløb </h2>
                     <br/><br/>
                     Det er ligetil at begynde hos os; du udfylder blot den nedenstående formular. Derefter får du
                     tilsendt et personligt login der giver dig tilgang på dit forløb.
                     <br/><br/>
                     Dit forløb indbefatter dine aftaler, hvilke øvelser du har aftalt med din fysoterapeut (ofte med videoreference), dine regninger, og meget mere. Du har også muligheden for at skrive direkte til din
                     direkte til din fysioterapeut, som vil blive notificeret og kan vende tilbage til dig ved lejlighed. Det er helt unikt system udviklet på baggrund af de erfaringer udvikleren
                     har haft med sin egen fysioterapeut.

                  </CellContent>
               </Cell>
            </div>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(FrontPage, styles));
