import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './training.scss';

import NavBar from '../../containers/navbar/NavBar';
import UserModule from '../../containers/usermodule/UserModule';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Training extends Component {
   render() {
      return (
         <div>
            Træning
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Training, styles));
