import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './admin.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Money extends Component {
   constructor() {
      super();
      this.state = {
         customers: [],
         menuSelected: 0
      };


      for(let i = 0; i < 15; i++) {
         this.state.customers.push({
            name: 'Lasse Jacobsen',
            date: '12-06-2016'
         });
      }
   }


   renderCustomerList() {
      // temp keys
      return this.state.customers.map((customer, index) => {
         return (
            <div key={index} styleName="column-item">
               <div>{customer.name}</div>
               <div>{customer.date}</div>
            </div>
         );
      });
   }

   render() {
      return (
         <div styleName="content" className="flex">
            <div className="list flex-1">
               <div styleName="column-header">
                  Kundeoversigt
               </div>
               {this.renderCustomerList()}
            </div>
            <div className="list flex-3">
               <div styleName="column-header">
                  Kundeoversigt: Lasse Jacobsen
               </div>
               {this.renderCustomerList()}
            </div>
            <div className="list flex-1 last">
               <div styleName="column-header">
                  Notifikationer
               </div>
               {this.renderCustomerList()}
            </div>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Money, styles, {allowMultiple: true}));
