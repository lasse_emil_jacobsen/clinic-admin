import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './admin.scss';

import Cell from '../../containers/cell/Cell';
import CellContent from '../../containers/cell/CellContent';

import Column from '../../containers/column/Column';
import ColumnItem from '../../containers/column/ColumnItem';

import Overview from './Overview';
import Customers from './Customers';
import Money from './Money';

import Accordion from '../../containers/accordion/Accordion';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Admin extends Component {
   constructor(props) {
      super(props);

      const menu = [
         'Oversigt',
         'Kunder',
         'Økonomi',
         'Statistik'
      ];

      this.state = {
         menu,
         menuSelected: 0
      };

   }

   navigate(index, e) {
      this.setState({
         menuSelected: index
      });
   }

   renderMenuItems() {
      return this.state.menu.map((item, index) => {
         let itemStyle = 'menu-item';
         let iconStyle = 'icon';

         if(this.state.menuSelected === index) {
            itemStyle += ' selected';
            iconStyle = 'selected-icon';
         }

         return (
            <div key={item} onClick={this.navigate.bind(this, index)} styleName={itemStyle}>
               <div styleName="text">{item}</div>
               <div styleName={iconStyle}><i className="material-icons">keyboard_arrow_right</i></div>
            </div>
         );
      });
   }

   renderSelection() {
      switch(this.state.menuSelected) {
         case 0:
         return <Overview/>;
         break;
         case 1:
         return <Customers/>;
         break;
         case 2:
         return <Money/>;
         break;
         default:
         return <div> No Route Selected </div>;
      }
   }

   render() {
      return (
         <div className="content-wrap">
            <div styleName="side-menu">
               <div styleName="menu-header"><i className="material-icons">menu</i></div>
               {this.renderMenuItems.call(this)}
            </div>
            {this.renderSelection()}
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Admin, styles, {allowMultiple: true}));
