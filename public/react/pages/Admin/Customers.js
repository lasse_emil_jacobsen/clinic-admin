import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './customers.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Customers extends Component {
   constructor() {
      super();
      this.state = {
         customers: [],
         menuSelected: 0
      };


      for(let i = 0; i < 15; i++) {
         this.state.customers.push({
            name: 'Lasse Jacobsen',
            date: '12-06-2016'
         });
      }
   }


   renderCustomerList() {
      // tmp keys
      return this.state.customers.map((customer, index) => {
         return (
            <div key={index} styleName="column-item">
               <div>{customer.name}</div>
               <div>{customer.date}</div>
            </div>
         );
      });
   }

   createCustomer() {
      this.setState({
         customers: this.state.customers.concat({
            name: 'Lasse Jacobsen',
            date: '12-06-2016'
         })
      });
   }

   render() {
      return (
         <div styleName="content" className="flex">
            <div className="list flex-1 last">
               <div styleName="column-header">
                  Kundeoversigt
               </div>
               {this.renderCustomerList()}
               <i onClick={this.createCustomer.bind(this)} className="material-icons">add</i>
            </div>

         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Customers, styles, {allowMultiple: true}));
