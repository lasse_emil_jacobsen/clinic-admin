import React from 'react';

const toggled = (haystack, needle) => {
  return haystack.find((hay) => {
    if(hay === needle) return true;
  }) > -1 ? true : false;
};

const addToToggled = (haystack, needle) => {
  if(toggled(haystack, needle) === false) {
    return haystack.concat(needle);
  } else {
    haystack.splice(haystack.indexOf(needle), 1);
    return haystack;
  }
};

export default class Accordion extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: [],
      items: []
    };

}

  componentDidMount() {
    const getElements = () => {
      return React.Children.map(this.props.children, (child, index) => {
        if(child.props.children) {
          return {
            parent: React.cloneElement(child, {
              children: [],
              onClick: this.toggle.bind(this, index)
            }),
            children: child.props.children,
            index
          };
        } else {
          return child;
        }
      });
    };

    this.setState({
      items: getElements()
    });

  }

  toggle(index) {
    this.setState({
      open: addToToggled(this.state.open, index)
    });
  }

  renderItems() {
    return this.state.items.map((item, index) => {
      if(!item.parent) {
        return item;
      }

      if(item.parent.props.className) {
        if(this.props.openClass && toggled(this.state.open, index)) {
          item.parent = React.cloneElement(item.parent, {
            className: item.parent.props.className.indexOf(this.props.openClass) > -1 ? item.parent.props.className : item.parent.props.className.trim() + ' ' + this.props.openClass
          });
        } else {
          item.parent = React.cloneElement(item.parent, {
            className: item.parent.props.className.split(this.props.openClass)[0]
          });
        }
      }

      return (
        <div key={index} style={{marginRight: 0}}>
           {item.parent}
          {toggled(this.state.open, index) &&
            <div className={this.props.lineBeforeChildren ? ' accordion-children' : ''}>
              <div className={'animate-fadein'}>
                {item.children}
              </div>
            </div>
          }
        </div>
      );
    });
  }

  render() {
    return (
      <div className="accordion-wrap">
        {this.renderItems.call(this)}
      </div>
    );
  }
}

Accordion.defaultProps = {
  lineBeforeChildren: true,
  openClass: 'open'
};
