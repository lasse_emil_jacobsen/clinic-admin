import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './tableofcontents.scss';

import Box from '../containers/box/Box.js';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class TableOfContents extends Component {
   render() {
      return (
         <Box columns={4}>

         </Box>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(TableOfContents, styles));
