const actions = {
   loginSuccess: (data) => {
      return {
         type: "LOGIN_SUCCESS",
         data
      };
   },
   loginFail: (data) => {
      return {
         type: "LOGIN_FAIL",
         data
      };
   },
   logoutSuccess: (data) => {
      return {
         type: "LOGOUT_SUCCESS",
         data
      };
   },
   logoutFail: (data) => {
      return {
         type: "LOGOUT_FAIL",
         data
      };
   },
   loginAsync: (data, dispatch) => {
      $.ajax({
         type: "POST",
         url: '/api/login',
         data: data,
         success: (resp) => {
            if(resp.success) {
               dispatch(actions.loginSuccess(resp.payload));
            } else {
               dispatch(actions.loginFail(resp.payload));
            }
         }
      });
   },
   logoutAsync: (data, dispatch) => {
      $.ajax({
         type: "POST",
         url: '/api/protected/logout',
         success: (resp) => {
            if(resp.success) {
               dispatch(actions.logoutSuccess(resp));
            } else {
               dispatch(actions.logoutFail(resp));
            }
         }
      });
   }
};

export default actions;
