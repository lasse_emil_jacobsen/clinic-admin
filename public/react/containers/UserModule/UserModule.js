import React, { Component } from 'react';
import Tipsy from 'react-tipsy';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './user.scss';

import Link from 'react-router/Link';

import actions from './actions';

const mapStateToProps = (state) => {
   return {
      user: state.user
   };
};

const mapDispatchToProps = (dispatch) => {
   return {
      loginAsync: (data) => actions.loginAsync(data, dispatch),
      logoutAsync: (data) => actions.logoutAsync(data, dispatch)
   };
};

class User extends Component {
   constructor(props) {
      super(props);

      this.state = {
         username: '',
         password: ''
      };
   }

   componentDidUpdate() {
      //console.log(this.props.user, 'user');
   }

   onChange(e) {
      switch(e.target.name) {
         case 'password':
            this.setState({
               password: e.target.value
            });
            break;
         case 'username':
            this.setState({
               username: e.target.value
            });
         break;
      }
   }

   login() {
      if(this.props.user.username) {
         this.props.logoutAsync({});
      } else {
         this.props.loginAsync({username: this.state.username, password: this.state.password});
      }
   }

   render() {
      return (
         <div styleName='user-module'>
            {this.props.user.admin === true &&
               <div styleName='personal'><Link to="/admin">ADMINISTRATION</Link></div>
            }
            {this.props.user.admin === false &&
               <div styleName='personal'><Link to="/forloeb">MIT FORLØB</Link></div>
            }
            {this.props.user.username &&
               <p styleName='text'> Velkommen, {this.props.user.username} </p>
            }
            {!this.props.user.username &&
               <div>
                  <input placeholder="Username" onChange={this.onChange.bind(this)} name="username" type="text" value={this.state.username}/>
                  <input placeholder="Password" onChange={this.onChange.bind(this)} name="password" type="password" value={this.state.password}/>
               </div>
            }
            <i styleName='account-icon' className="material-icons md-30 md-light" onClick={this.login.bind(this)}>account_circle</i>
         </div>
      );
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(User, styles, {allowMultiple: true}));
