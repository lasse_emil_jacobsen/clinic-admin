const reducer = (state = {}, action) => {
   switch(action.type) {
      case 'LOGIN_SUCCESS':
         return action.data;
         break;
      case 'LOGIN_FAIL':
         return action.data;
         break;
      case 'LOGOUT_SUCCESS':
         return action.data;
         break;
      case 'LOGOUT_FAIL':
         return action.data;
         break;
      default:
         return state;
   }
};

export default reducer;
