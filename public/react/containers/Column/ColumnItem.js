import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './column.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class ColumnItem extends Component {
   render() {
      return (
         <div styleName='column-item'>
            {this.props.title}
         </div>
      );
   }
}

ColumnItem.defaultProps = {
   title: 'placeholder'
};

export default connect(mapStateToProps, null)(CSSModules(ColumnItem, styles, {allowMultiple: true}));
