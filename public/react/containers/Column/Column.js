import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './column.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Column extends Component {
   render() {
      return (
         <div styleName='column-header'>
            {this.props.title}
         </div>
      );
   }
}


Column.defaultProps = {
   title: 'placeholder'
};


export default connect(mapStateToProps, null)(CSSModules(Column, styles, {allowMultiple: true}));
