import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './cell.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Cell extends Component {
   render() {
      return (
         <div styleName="cell" className={'fadein col-' + this.props.cols}>
            {this.props.children}
         </div>
      );
   }
}



Cell.defaultProps = {
   cols: '12'
};

/*export default {
   Cell: connect(mapStateToProps, null)(CSSModules(Cell, styles, {allowMultiple: true})),
   CellContent: connect(mapStateToProps, null)(CSSModules(CellContent, styles, {allowMultiple: true}))
};*/

export default connect(mapStateToProps, null)(CSSModules(Cell, styles, {allowMultiple: true}));
