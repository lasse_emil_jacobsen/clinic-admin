import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './cell.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class CellContent extends Component {
   render() {
      return (
         <div styleName="cell-content">
            <div> {this.props.children} </div>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(CellContent, styles, {allowMultiple: true}));
