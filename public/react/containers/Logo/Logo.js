import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './logo.scss';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Logo extends Component {
   render() {
      return (
         <div styleName='logo-backdrop'>
            <h1>
               SØERNES FYSIOTERAPI
            </h1>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Logo, styles));
