import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './navbar.scss';

import Link from 'react-router/Link';

const mapStateToProps = (state) => {
   return {
      user: state.user
   };
};

class NavBar extends Component {
   render() {
      return (
         <div styleName='navbar'>
            <div><Link to="/">BEGYND HER</Link></div>
            <div><Link to="/programmer">TRÆNINGSPROGRAMMER</Link></div>
            <div><Link to="/kontakt">KONTAKT</Link></div>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(NavBar, styles));
