import React, { Component } from 'react';
import { connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './header.scss';

import NavBar from '../navbar/NavBar';
import UserModule from '../usermodule/UserModule';
import Logo from '../logo/Logo';

const mapStateToProps = (state) => {
   return {
      user: state.data.user
   };
};

class Header extends Component {
   render() {
      return (
         <div styleName='wrapper'>
            <Logo/>
            <NavBar/>
            <UserModule/>
         </div>
      );
   }
}

export default connect(mapStateToProps, null)(CSSModules(Header, styles));
