const security = require('../lib/security.js');
const { validate, validator } = require(global.__root + '/lib/validator.js');

const mongo = require('mongojs');
const db = mongo('skullcave', ['users']);

const operations = {
	createUser: (user, isAdmin) => {
		return new Promise((resolve, reject) => {

			const errors = validateOperations.createUser(user);

			if (errors.length) {
				reject(errors);
				return;
			}

			user.admin = !!isAdmin;

			security.createSaltedHash(user.password).then(passObj => {
				user.password = passObj.hash;
				db.users.insert(user, (err, res) => {
					if (err) {
						reject(err);
					} else {
						resolve(res);
					}
				});
			}).catch(err => reject(err));

		});
	},
	fetchUser: (username, password) => {
		return new Promise((resolve, reject) => {
			db.users.find({username: username}, (err, res) => {
				if(err) {
					reject(`Couldn't find user ${username}: ${err}`);
				} else {
					resolve(res[0]);
				}
			});
		}).then(user => {
			return security.createHash(password, user.password.substring(0, 29)).then(hash => {
				return Promise.resolve({user, hashedPassword: hash});
			}).catch(err => Promise.reject(err));
		}).then(userObj => {
			if(userObj.user.password === userObj.hashedPassword) {
				return Promise.resolve(userObj.user);
			} else {
				return Promise.reject("Couldn't find user ${username} with pw ${password}");
			}
		});
	},
	setPassword: (id, password) => {
		return new Promise((resolve, reject) => {
			db.users.update({ _id: id }, { password: password }, (err, res) => {
				if (err) reject(err);
				resolve(res);
			});
		});
	}
};

const validateOperations = {
	createUser: (user) => {

		if (!user) return ['User is undefined'];

		const requiredProps = {
			password: {
				validate: (password) => validator.isLength(password, 5, 100) && validator.isAlphanumeric(password) && validator.matches(password, /[\d]/),
				error: 'Password must be between 5-100 characters long and contain at least 1 number'
			},
			username: {
				validate: (username) => validator.isLength(username, 5, 30),
				error: 'Username must be between 5-30 characters long'
			},
			email: {
				validate: (email) => validator.isEmail(email),
				error: 'Email must be valid',
				required: true
			}
		};

		const errors = validate(user, requiredProps);

		return errors;
	}
};

module.exports = operations;

/* Dummy admin user creation below */
db.users.ensureIndex({username: 1}, {unique: 1}, (err, res) => {
	if(err) {
		console.log("mongo error");
	} else {
		const testUser = {
			username: 'Admin',
			password: '123456',
			email: 'lolz@gmail.com',
		};

		operations.createUser(testUser, true).then((res) => {
			console.log(res);
		}).catch(err => console.log("user likely already defined"));
	}
});



