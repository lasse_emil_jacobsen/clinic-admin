const mongo = require('mongojs');
const db = mongo('skullcave', ['errors', 'history']);

const operations = {
   saveErrors: (errors) => {
      return new Promise((resolve, reject) => {
         db.errors.insert(errors, (err, doc) => {
            if(err) reject(err);
            else resolve(doc);
         });
      });
   },
   saveHistory: (history) => {
      return new Promise((resolve, reject) => {
         db.history.insert(history, (err, doc) => {
            if(err) reject(err);
            else resolve(doc);
         });
      });
   }
};

module.exports = operations;
