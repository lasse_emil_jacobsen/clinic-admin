const cluster = require('cluster');

global.__root = __dirname;
global.log = require(global.__root + '/lib/logger.js');

if (cluster.isMaster) {
	const cpuCount = require('os').cpus().length;

	for (let i = 0; i < cpuCount; i++) {
		cluster.fork();
	}

	cluster.on('exit', () => {
		cluster.fork();
	});

} else {
	// The server (worker clusters):
	const express = require('express');
	const app = express();
	const bodyparser = require('body-parser');
   	const cookieparser = require('cookie-parser');

	const pages = require(__dirname + '/server/routes/pages.js');
	const api = require(__dirname + '/server/routes/api.js');

	// Middleware
	app.use(bodyparser.json());
  	app.use(bodyparser.urlencoded({ extended: false }));
  	app.use(cookieparser());
	app.use(express.static('public'));

	app.use('*', (req, res, next) => {
		console.log(req.baseUrl);
		console.log(req.body);
		next();
	});

	app.get('*', pages);

	app.use('/api', api);

	/*app.post('/', (req, res) => {
		global.log.emptyBatches().then((res) => {
			console.log(res);
		}).catch(err => console.log(err));
		res.send('heh');
	});*/

	// next(err) will be caught here, make sure err has a name property.
	app.use((err, req, res, next) => {
		console.log(err, 'err');
		switch(err.name) {
			case 'UnauthorizedError':
				res.status(401).send("Invalid token");
			break;
			case 'RevokedTokenError':
				res.status(401).send("Revoked token");
			break;
			default:
				res.status(401).send("Invalid request");
			break;
		}
	});

	app.listen(3000, () => {
		console.log("Cluster", cluster.worker.id, ": listening on 3k");
	});

}

process.on('uncaughtException', (err) => {
	global.log.error(err.stack);
	process.exit(1);
});
