const jwt = require('jsonwebtoken');

const db = require(global.__root + '/mongo/users.js');

const operations = {
   getUser: (username, password) => {
      return db.fetchUser(username, password);
   },
   createUser: (user, isAdmin) => {
      return db.createUser(user, isAdmin);
   }
};

module.exports = operations;
