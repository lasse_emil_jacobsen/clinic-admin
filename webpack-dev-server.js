const WebpackDevServer = require("webpack-dev-server");
const webpack = require("webpack");
const config = require('./webpack.config.js');

const rimraf = require('rimraf');

rimraf(__dirname + '/public/build', (err) => {
   if(err) {
      console.log("Couldn't remove build folder:" + err);
      process.exit(1);
   } else {
      console.log("Build folder removed");
   }

   config[0].entry.app.unshift("webpack-dev-server/client?http://localhost:8081/", "webpack/hot/dev-server");
   config[0].plugins = [new webpack.HotModuleReplacementPlugin()];
   config[0].output.publicPath = 'http://localhost:8081/public/build/';
   /*config[0].module.loaders.unshift({
         test: /\.jsx?$/,
         loaders: ['react-hot'],
         include: path.join(__dirname, 'public')
   });*/

   const compiler = webpack(config);

   const server = new WebpackDevServer(compiler, {
      filename: 'bundle.js',
      publicPath: 'http://localhost:8081/public/build/',
      hot: true,
      stats: {
         colors: true
      }
   });

   server.listen(8081, "localhost", function(err) {
      if(!err) {
         console.log("Server ready!");
      } else {
         console.log("Problem running hot mode: " + err);
         process.exit(1);
      }
   });


});
